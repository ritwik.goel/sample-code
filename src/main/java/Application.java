import database.helper.configFactory.ConfigManager;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

public class Application extends AbstractVerticle {
    @Override
    public void start() throws Exception {
        super.start();
        VertxOptions vertxOptions=new VertxOptions();

        ConfigManager.INSTANCE.setMainConfig(config());

        vertx.deployVerticle(HttpVerticle.class.getName());
    }
}
