package controller;

import apiCallData.CreditCardImpl;
import apiCallData.ThirdPartyAPI;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;

public enum ApiRouter {
    INSTANCE;

    public Router router(Vertx vertx){
        Router router = Router.router(vertx);

        router.put("/creditCard").handler(CreditCardImpl.INSTANCE::creditCardDto);
        router.put("/currencyConverter").handler(ThirdPartyAPI.INSTANCE::convertAmount);
        return router;
    }
}
