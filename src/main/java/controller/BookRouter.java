package controller;

import data.BookList;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;

public enum BookRouter {
    INSTANCE;
    public Router router(Vertx vertx){
        Router router = Router.router(vertx);

        router.get("/getBooks").handler(BookList.INSTANCE::getBooks);
        router.post("/addBooks").handler(BookList.INSTANCE::addBooks);
        router.put("/updateBooks/:bid").handler(BookList.INSTANCE::updateBooks);
        router.delete("/deleteBooks/:bid").handler(BookList.INSTANCE::deleteBook);
        return router;
    }
}
