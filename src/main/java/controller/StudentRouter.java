package controller;

import data.StudentList;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;
public enum StudentRouter {
    INSTANCE;
    public Router router(Vertx vertx){
        Router router =Router.router(vertx);

        router.get("/getStudent").handler(StudentList.INSTANCE::getStudent);
        router.get("/getStudentById/:sid").handler(StudentList.INSTANCE::getStudentById);
        router.post("/addStudent").handler(StudentList.INSTANCE::addStudent);
        router.put("/updateStudent/:sid").handler(StudentList.INSTANCE::updateStudent);
        router.delete("/deleteStudent/:sid").handler(StudentList.INSTANCE::deleteStudent);

        return router;
    }
}
