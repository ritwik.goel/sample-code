package controller;

import apiCallData.ThirdPartyAPI;
import data.AuthorList;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;

public enum AuthorRouter {
    INSTANCE;

    public Router router(Vertx vertx){
        Router router = Router.router(vertx);


        router.get("/getAuthor").handler(AuthorList.INSTANCE::getAuthor);
        router.get("/getAuthorById/:srnum").handler(AuthorList.INSTANCE::getAuthorById);
        router.post("/addAuthor").handler(AuthorList.INSTANCE::addAuthor);
        router.put("/updateAuthor/:srnum").handler(AuthorList.INSTANCE::updateAuthor);
        router.delete("/deleteAuthor/:srnum").handler(AuthorList.INSTANCE::deleteAuthor);

        return router;
    }
}
