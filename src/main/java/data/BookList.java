package data;

import database.SqlConfigFactory;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import models.Book;
import models.Student;
import utils.ErrorResponse;
import utils.ResponseWriter;

import java.util.ArrayList;
import java.util.List;

public enum BookList {
    INSTANCE;

    public void getBooks(RoutingContext routingContext){
        List<BookData>books=new ArrayList<>();
        for (Book book:SqlConfigFactory.MASTER.getServer().find(Book.class).findList()){
            books.add(new BookData(book));
        }
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,books);
    }

    public void addBooks(RoutingContext routingContext){
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        Book book=new Book();

        book.setBid(bodyAsJson.getInteger("bid"));
        book.setPublisher(bodyAsJson.getString("publisher"));
        book.setAuthor(bodyAsJson.getString("author"));
        book.setReleaseDate(bodyAsJson.getString("releaseDate"));

        book.save();
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new BookData(book));
    }

    public void updateBooks(RoutingContext routingContext){

        String bid = routingContext.pathParam("bid");
        JsonObject bodyAsJson = routingContext.getBodyAsJson();

        Book book1 = SqlConfigFactory.MASTER.getServer().find(Book.class).where().eq("bid", bid).findOne();
            if (book1==null){
                ResponseWriter.INSTANCE.writeErrorJsonResponse(routingContext,new ErrorResponse("Invalid book id not find"));
            }
        book1.setBid(bodyAsJson.getInteger("bid"));
        book1.setPublisher(bodyAsJson.getString("publisher"));
        book1.setAuthor(bodyAsJson.getString("author"));
        book1.setReleaseDate(bodyAsJson.getString("releaseDate"));

        book1.update();
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new BookData(book1));
    }

    public void deleteBook(RoutingContext routingContext){

        String bid = routingContext.pathParam("bid");
        Book book = SqlConfigFactory.MASTER.getServer().find(Book.class).where().eq("bid", bid).findOne();
        if (book==null){
            ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new ErrorResponse("Invalid book id not found"));
        }
        book.delete();
        //System.out.println("Delete function call");
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new SuccessResponse());
    }

    private class BookData{
        private int bid;
        private String author;
        private String publisher;
        private String releaseDate;

        public BookData(Book book) {
            this.bid = book.getBid();
            this.author=book.getAuthor();
            this.publisher = book.getPublisher();
            this.releaseDate=book.getReleaseDate();
        }
    }
}
