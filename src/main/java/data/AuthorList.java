package data;

import database.SqlConfigFactory;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import lombok.Data;
import models.Author;
import utils.ErrorResponse;
import utils.ResponseWriter;

import java.util.ArrayList;
import java.util.List;

public enum AuthorList {
    INSTANCE;
    /**
     * getMethod to retrieve all Author data from db
     */
    public void getAuthor(RoutingContext routingContext) {
        List<AuthorData> authors=new ArrayList<>();
        for (Author author: SqlConfigFactory.MASTER.getServer().find(Author.class).findList()){
            authors.add(new AuthorData(author));
        }
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,authors);
    }

    /**
     * getMethod to retrieve all Author by ID from database.
     */
    public void getAuthorById(RoutingContext routingContext){
        String srnum = routingContext.pathParam("srnum");
        Author author = SqlConfigFactory.MASTER.getServer().find(Author.class).where().eq("srnum", srnum).findOne();
        if (author==null){
            ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new ErrorResponse("Wrong Sr. Number or data not valid"));
        }
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new AuthorData(author));
    }

    /**
     * postMethod to create new Author in database.
     */
    public void addAuthor(RoutingContext routingContext){
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        Author author=new Author();

        author.setSrnum(bodyAsJson.getInteger("srnum"));
        author.setTitle(bodyAsJson.getString("title"));
        author.setCategory(bodyAsJson.getString("category"));
        author.setPublishedDate(bodyAsJson.getString("publishedDate"));
        author.setPrice(bodyAsJson.getDouble("price"));
        author.setActive(bodyAsJson.getBoolean("active"));

        author.save();
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new AuthorData(author));
    }

    /**
     * putMethod to update Author by id to correction some things as per requirement
     */
    public void updateAuthor(RoutingContext routingContext){
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        String srnum = routingContext.pathParam("srnum");

        Author author = SqlConfigFactory.MASTER.getServer().find(Author.class).where().eq("srnum", srnum).findOne();

        if (author==null){
            ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new ErrorResponse("This Sr number is not valid "));
        }
        author.setSrnum(bodyAsJson.getInteger("srnum"));
        author.setTitle(bodyAsJson.getString("title"));
        author.setCategory(bodyAsJson.getString("category"));
        author.setPublishedDate(bodyAsJson.getString("publishedDate"));
        author.setPrice(bodyAsJson.getDouble("price"));
        author.setActive(bodyAsJson.getBoolean("active"));

        //add new
        //Double convertor = convertor(author.getPrice());
        //System.out.println("Converted value from add method : "+convertor);
        author.update();
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new AuthorData(author));
    }


    /**
     * deleteMethod to delete data from db
     */
    public void deleteAuthor(RoutingContext routingContext){
        String srnum = routingContext.pathParam("srnum");
        Author author = SqlConfigFactory.MASTER.getServer().find(Author.class).where().eq("srnum", srnum).findOne();
        if (author==null){
            ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new ErrorResponse("This Sr number is not valid for delete"));
        }
        author.delete();
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new SuccessResponse());
    }


    @Data
    private  class AuthorData{
        private int srnum;
        private String title;
        private  String category;
        private  String publishedDate;
        private Double price;
        private Boolean active;

        public AuthorData(Author author) {
            this.srnum = author.getSrnum();
            this.title = author.getTitle();
            this.category = author.getCategory();
            this.publishedDate = author.getPublishedDate();
            this.price = author.getPrice();
            this.active = author.getActive();
        }
    }
}
