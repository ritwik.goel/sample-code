package data;

import com.google.gson.Gson;
import database.SqlConfigFactory;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import lombok.Data;
import models.Student;
import utils.ErrorResponse;
import utils.ResponseWriter;

import java.util.ArrayList;
import java.util.List;

public enum StudentList {
    INSTANCE;

    /**
     * getMethod to retrieve all student data from db
     * @param routingContext
     */
    public void getStudent(RoutingContext routingContext) {
        List<StudentData> students =new ArrayList<>();
        for (Student student: SqlConfigFactory.MASTER.getServer().find(Student.class).findList()){
            students.add(new StudentData(student));
        }
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,students);
    }

    /**
     * getMethod by id which is fetching data as per given id.
     * @param routingContext
     */
    public void getStudentById(RoutingContext routingContext) {
        String sid = routingContext.pathParam("sid");
        Student student = SqlConfigFactory.MASTER.getServer().find(Student.class).where().eq("sid", sid).findOne();
        if (student==null){
            ResponseWriter.INSTANCE.writeErrorJsonResponse(routingContext,new ErrorResponse("Invalid student id not found"));
        }
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new StudentData(student));
    }

    /**
     * postMethod to create new record in database.
     * @param routingContext
     */
    public void addStudent(RoutingContext routingContext) {

        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        Student student=new Student();
        student.setSid(bodyAsJson.getInteger("sid"));
        student.setSname(bodyAsJson.getString("sname"));
        student.setSloc(bodyAsJson.getString("sloc"));

        student.save();

        ResponseWriter.INSTANCE.writeJsonResponse(routingContext, new StudentData(student));
    }

    /**
     * putMethod for update student details as per requirement
     * @param routingContext
     */
    public void updateStudent(RoutingContext routingContext) {

        String sid = routingContext.pathParam("sid");
        JsonObject bodyAsJson = routingContext.getBodyAsJson();

        Student student1 = SqlConfigFactory.MASTER.getServer().find(Student.class).where().eq("sid", sid).findOne();
        if (student1==null){
            ResponseWriter.INSTANCE.writeErrorJsonResponse(routingContext,new ErrorResponse("Invalid Student id not Found"));
        }
        student1.setSid(bodyAsJson.getInteger("sid"));
        student1.setSname(bodyAsJson.getString("sname"));
        student1.setSloc(bodyAsJson.getString("sloc"));

        student1.update();

        ResponseWriter.INSTANCE.writeJsonResponse(routingContext, new StudentData(student1));
    }

    /**
     * deleteMethod to remove record from database.
      * @param routingContext
     */
    public void deleteStudent(RoutingContext routingContext) {

        String sid = routingContext.pathParam("sid");
        Student student1 = SqlConfigFactory.MASTER.getServer().find(Student.class).where().eq("sid", sid).findOne();
        if (student1==null){
            ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new ErrorResponse("Invalid Student id not found"));
        }
        student1.delete();
        System.out.println("Delete call");
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext, new SuccessResponse());
    }
    @Data
    private class StudentData{
        private int sid;
        private String sname;
        private String sloc;

        public StudentData(Student student){
         this.sid=student.getSid();
         this.sname=student.getSname();
         this.sloc=student.getSloc();
        }
    }
}
