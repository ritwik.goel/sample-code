package apiCall;

import lombok.Data;

@Data
public class CurrencyEntity {
    private Double orgAmount;
    private Double convertedAmount;

    public CurrencyEntity(Double orgAmount, Double convertedAmount) {
        this.orgAmount = orgAmount;
        this.convertedAmount = convertedAmount;
    }
}
