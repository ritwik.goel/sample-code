package utils;

import com.google.gson.Gson;

public enum GsonMapper {
    INSTANCE;

    private Gson gson;
    public Gson getMapper(){
        if(gson==null){
            gson=new Gson();
        }
        return gson;
    }
}
