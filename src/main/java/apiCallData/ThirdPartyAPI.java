package apiCallData;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import apiCall.CreditEntity;
import apiCall.CurrencyEntity;
import data.WeatherResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import utils.ResponseWriter;

public enum ThirdPartyAPI {
    INSTANCE;
    /**
     * putMethod to converted price into as per requirement currencies.
     */
    public void convertAmount(RoutingContext routingContext){
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        Double price1 = bodyAsJson.getDouble("price");
        Double convertor1 = convertor(price1);

        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new CurrencyEntity(price1,convertor1));
    }
    public Double convertor(Double usdPrice){
        try{
            HttpResponse<String> response = Unirest.get("https://api.apilayer.com/fixer/convert?from=USD&to=INR&amount="+usdPrice)
                    .header("apikey", "KyTrYNs2ZSlCUN4wbgQV5432QNLsMqc4")
                    .asString();
            if(response.getStatus()==200){
                return  new JsonObject(response.getBody()).getDouble("result");
            }
            return usdPrice;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return usdPrice;
    }

    public void showWeather(RoutingContext routingContext){
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        String sunrise1 = bodyAsJson.getString("sunrise");
        String resSunrise = weather(sunrise1);
        String sunset1 = bodyAsJson.getString("sunset");
        String resSunset = weather(sunset1);
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new WeatherResponse(resSunrise,resSunset));
    }

    public String weather(String city){
        try{
            Unirest.setTimeouts(0, 0);
            HttpResponse<String> response = Unirest.get("https://sunrise-sunset-times.p.rapidapi.com/getSunriseAndSunset?date=2022-09-20&latitude=28.644800&longitude=77.216721&timeZoneId="+city)
                    .header("X-RapidAPI-Key", "b17f058ad9msh91a954a8dc0795fp194fd0jsn28dcf001eee2")
                    .header("X-RapidAPI-Host", "sunrise-sunset-times.p.rapidapi.com")
                    .asString();

            if (response.getStatus()==200){
                return  new JsonObject(response.getBody()).getString("result");
            }
            return city;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return city;
    }

    public void covidRes(RoutingContext routingContext){

        //ResponseWriter.INSTANCE.writeJsonResponse(routingContext,new WeatherResponse(covid1));
    }
    public String covid(String city){
        try{
            HttpResponse<String> response = Unirest.get("https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/total?country="+city)
                    .header("X-RapidAPI-Key", "b17f058ad9msh91a954a8dc0795fp194fd0jsn28dcf001eee2")
                    .header("X-RapidAPI-Host", "covid-19-coronavirus-statistics.p.rapidapi.com")
                    .asString();

            if (response.getStatus()==200){
                return  new JsonObject(response.getBody()).getString("result");
            }
            return city;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return city;
    }

    public void creditCardDto(RoutingContext routingContext){
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        String visaType = bodyAsJson.getString("visaType");

        CreditEntity storeData = creditCard(visaType);
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,storeData);
    }
    public CreditEntity creditCard(String visaType){
        try{
            HttpResponse<String> response = Unirest.get("https://fake-valid-cc-data-generator.p.rapidapi.com/request/?visa_type="+visaType)
                    .header("X-RapidAPI-Key", "b17f058ad9msh91a954a8dc0795fp194fd0jsn28dcf001eee2")
                    .header("X-RapidAPI-Host", "fake-valid-cc-data-generator.p.rapidapi.com")
                    .asString();
            if (response.getStatus()==200){
                return new Gson().fromJson(response.getBody(), CreditEntity.class);
            }
            return new CreditEntity();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return  new CreditEntity();
    }

}
