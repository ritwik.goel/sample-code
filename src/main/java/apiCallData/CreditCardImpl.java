package apiCallData;

import apiCall.CreditEntity;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import utils.ResponseWriter;

public enum CreditCardImpl {
    INSTANCE;

    public void creditCardDto(RoutingContext routingContext){
        JsonObject bodyAsJson = routingContext.getBodyAsJson();
        String visaType = bodyAsJson.getString("visaType");

        CreditEntity storeData = creditCard(visaType);
        ResponseWriter.INSTANCE.writeJsonResponse(routingContext,storeData);
    }
    public CreditEntity creditCard(String visaType){
        try{
            HttpResponse<String> response = Unirest.get("https://fake-valid-cc-data-generator.p.rapidapi.com/request/?visa_type="+visaType)
                    .header("X-RapidAPI-Key", "b17f058ad9msh91a954a8dc0795fp194fd0jsn28dcf001eee2")
                    .header("X-RapidAPI-Host", "fake-valid-cc-data-generator.p.rapidapi.com")
                    .asString();
            if (response.getStatus()==200){
                return new Gson().fromJson(response.getBody(), CreditEntity.class);
            }
            return new CreditEntity();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return  new CreditEntity();
    }
}
