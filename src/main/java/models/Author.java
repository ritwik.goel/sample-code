package models;

import database.SqlConfigFactory;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AuthorDetails")
@Data
public class Author {
    @Id
    private int srnum;
    private String title;
    private String category;
    private String publishedDate;
    private Double price;
    private Boolean active;

    public void save() {
        SqlConfigFactory.MASTER.getServer().save(this);
    }

    public void update() {
        SqlConfigFactory.MASTER.getServer().update(this);
    }

    public void delete() {
        SqlConfigFactory.MASTER.getServer().delete(this);
    }
}
