package models;

import database.SqlConfigFactory;
import io.ebean.annotation.Index;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BooksDetails")
@Data
public class Book {
    @Id
    @Index
    private int bid;
    private String author;
    private String publisher;
    private String releaseDate;

    public void save(){
        SqlConfigFactory.MASTER.getServer().save(this);
    }
    public void update(){
        SqlConfigFactory.MASTER.getServer().update(this);
    }
    public void delete(){
        SqlConfigFactory.MASTER.getServer().delete(this);
    }

}
